# Hackalod 2020

Scripts and data that were created during [Hackalod 2020](https://www.netwerkdigitaalerfgoed.nl/events/hackalod-2020/). For more information, read this [blog post](https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/events/hackalod2020/). 