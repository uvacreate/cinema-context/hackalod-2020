import json
import pandas as pd

from rdflib import Graph, URIRef, Literal, RDF, DC, DCTERMS, XSD, Namespace

# pip install git+https://github.com/LvanWissen/RDFAlchemy.git
from rdfalchemy import rdfSubject, rdfSingle

SCHEMA = Namespace("http://schema.org/")
AATPOSTER = URIRef("http://vocab.getty.edu/aat/300027221")


class Poster(rdfSubject):
    rdf_type = SCHEMA.Poster

    name = rdfSingle(SCHEMA.name)

    additionalType = rdfSingle(SCHEMA.additionalType)

    creator = rdfSingle(SCHEMA.creator)

    dateCreated = rdfSingle(SCHEMA.dateCreated)

    about = rdfSingle(SCHEMA.about)
    image = rdfSingle(SCHEMA.image)


# Geheugen van Nederland data
# Zie: https://gist.github.com/LvanWissen/1aa74dee61e85923826b6372a24f9367
# Of: eye_afficheproject.py
with open('data/eye_afficheproject.json') as infile:
    AFFICHEDATA = json.load(infile)

# Affichekoppelingen Cinema Context
df = pd.read_csv('data/affichekoppelingen2020.csv')
CCDATA = df.where(pd.notnull(df), None)


def main(data, destination):

    g = rdfSubject.db = Graph(
        identifier=
        "https://data.create.humanities.uva.nl/id/cinemacontext/affiches/")

    uri2cc = dict()
    for d in CCDATA.to_dict(orient='records'):
        if d['check'] in ('x', 'te nieuw'):
            continue

        if d['check'] and d['check'].startswith('F'):
            cc = URIRef("http://www.cinemacontext.nl/id/" + d['check'])
        else:
            cc = URIRef(d['ccuri'])

        uri2cc[d['permalink']] = cc

    for uri, a in [i for i in data.items()]:

        # Alleen EYE posters
        if 'EYE' not in a['objectlevelId']:
            continue

        permalink = "https://resolver.kb.nl/resolve?urn=" + uri

        affiche = Poster(URIRef(permalink),
                         name=a['title'],
                         additionalType=AATPOSTER,
                         creator=a['creator'],
                         about=uri2cc.get(permalink),
                         image=URIRef(a['thumbnail']))

        if a['startDate']:
            date = Literal(a['startDate'][:4],
                           datatype=XSD.gYear,
                           normalize=False)
            affiche.dateCreated = date

    g.bind('xsd', XSD)
    g.bind('schema', SCHEMA)

    g.serialize(destination, format='trig')


if __name__ == "__main__":
    main(data=AFFICHEDATA, destination='affiches_hackalod2020.trig')
